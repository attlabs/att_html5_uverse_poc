/**
 * @author julia_jacobs@labs.att.com (Julia Jacobs)
 * Date: 5/22/13
 * Time: 10:04 AM
 * @file server.js
 * @desc Node express / http server initialization, configuration and routing.
 * Uses a number of node plugins including consolidate for haml compiling,
 * less-middleware for LESS compiling and walk for local file / directory
 * information.
 */

'use strict';

/** node modules */
var express = require('express'),
  http = require('http'),
  path = require('path'),
  engines = require('consolidate'),
  lessMiddleware = require('less-middleware'),
  fs = require('fs'),
  walk = require('walk'),
  app = express();

app.engine('haml', engines.haml);

/** express confiuration */
app.configure(function () {
  app.set('env', process.env.NODE_ENV || 'development');
  app.set('port', process.env.PORT || 5000);
  app.set('view engine', 'haml');
  app.set('views', __dirname + '/views');
  app.use(express.favicon(__dirname + '/public/favicon.ico'));
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

/** development environment configuration */
app.configure('development', function () {
  app.use(express.errorHandler({
                                 dumpExceptions: true,
                                 showStack: true
                               }));
  app.use(lessMiddleware({
                           dest: __dirname + '/public/css',
                           src: __dirname + '/public/less',
                           compress: true
                         }));
});

/** production environment configuration */
app.configure('production', function () {
  app.use(express.errorHandler());
});

/** global routing - applies to all uris */
app.options('*', function (req, res) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Credentials', true);
  // try: 'POST, GET, PUT, DELETE, OPTIONS'
  res.header('Access-Control-Allow-Methods',
             'POST, GET, PUT, DELETE, OPTIONS');
  // try: 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept'
  res.header('Access-Control-Allow-Headers', 'Content-Type');
});

/** app router */
app.get('/app', function (req, res) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Credentials', true);
  // try: 'POST, GET, PUT, DELETE, OPTIONS'
  res.header('Access-Control-Allow-Methods',
             'POST, GET, PUT, DELETE, OPTIONS');
  // try: 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept'
  res.header('Access-Control-Allow-Headers', 'Content-Type');

  // renders proper layout file based on environment
  if (process.env.NODE_ENV === "'production'" || process.env.NODE_ENV ===
                                                 'production') {
    res.render('index');
  } else {
    res.render('index-dev');
  }
});

/** test page to test individual js libraries / functions outside of app */
app.get('/test', function (req, res) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Credentials', true);
  // try: 'POST, GET, PUT, DELETE, OPTIONS'
  res.header('Access-Control-Allow-Methods',
             'POST, GET, PUT, DELETE, OPTIONS');
  // try: 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept'
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  res.render('test');
});

/** gateway page */
app.get('/', function (req, res) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Credentials', true);
  // try: 'POST, GET, PUT, DELETE, OPTIONS'
  res.header('Access-Control-Allow-Methods',
             'POST, GET, PUT, DELETE, OPTIONS');
  // try: 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept'
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  res.render('device_test');
});

/** gateway page */
app.get('../public/docs', function (req, res) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Credentials', true);
  // try: 'POST, GET, PUT, DELETE, OPTIONS'
  res.header('Access-Control-Allow-Methods',
             'POST, GET, PUT, DELETE, OPTIONS');
  // try: 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept'
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  res.render('device_test');
});

http.createServer(app).listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port') + ' in ' + process.env.NODE_ENV + ' mode.');

  /** @todo create node module to create namespace file */
  var emitter,
    /** string used for app global namespace */
    str,
    iter = [],
    /** Path of app directory. */
    startPath = 'public/javascripts/poc',
    /** Name of app. */
    appName = 'poc',
    author = 'Julia Jacobs',
    desc = 'Application global namespaces.',
    authorEmail = 'julia_jacobs@labs.att.com',
    /** Path of generated namespace file. */
    filePath = 'public/javascripts/poc/namespace.js',
    /** creates file */
    log = fs.createWriteStream(filePath, {
      'flags': 'w'
    }),
    currentFormattedDate = function() {
      var d = new Date(),
        curr_date = d.getDate(),
        curr_month = d.getMonth(),
        curr_year = d.getFullYear();
      curr_month++;

      return curr_month + "/" + curr_date + "/" + curr_year;
    },
    currentFormattedTime = function() {
      var currentTime = new Date(),
        hours = currentTime.getHours(),
        minutes = currentTime.getMinutes(),
        suffix = "AM";

      if (minutes < 10) {
        minutes = "0" + minutes
      }

      if (hours >= 12) {
        suffix = "PM";
        hours = hours - 12;
      }
      if (hours == 0) {
        hours = 12;
      }

      return hours + ":" + minutes + " " + suffix;
    },
    generatedFileName = filePath.split('/').slice(-1)[0],
    spacedStartPath = startPath.replace(/\//g, ' '),
    date = new Date(),
    jsdocFileBlock = "/**\n" +
      "* @author " + authorEmail + " (" + author + ")\n" +
      "* Date: " + currentFormattedDate() + "\n" +
      "* Time: " + currentFormattedTime() + "\n" +
      "* @file " + generatedFileName + "\n" +
      "* @desc " + desc + "\n*/\n\n// 'use strict';\n";

  /**
   * @desc Gets rid of " = {}" to isolate namespace name for jsdocblock.
   * @function stripFromNamespaceString
   * @inner
   * @param {string} namespaceStr Namespace string.
   * @return {string}
   */
  function stripFromNamespaceString(namespaceStr) {
    if (typeof namespaceStr === 'string') {
      return (namespaceStr.indexOf('||') !== -1) ? appName : namespaceStr.replace(' = {}', '');
    } else {
      return '';
    }
  }

  /**
   * @desc Gets the parent namespace name for jsdocblock.
   * @function getParentNamespace
   * @inner
   * @param {string} namespaceStr Namespace string.
   * @param {string} previousNamespaceString Name of previously declared namespace.
   * @return {string}
   */
  function getParentNamespace(namespaceStr, previousNamespaceString) {
    var namespaceArr = namespaceStr.split('.');

    if (typeof namespaceStr === 'string' && typeof previousNamespaceString === 'string') {
      return namespaceArr.slice(0, -1).join('.');
    }
  }

  /**
   * @desc Create the namespace jsdocblock.
   * @function createDocBloc
   * @inner
   * @param {string} namespace Name of namespace.
   * @param {string} previousNamespace Name of previously declared namespace.
   * @return {string}
   */
  function createDocBloc(namespace, previousNamespace) {
    var strippedNamespace = stripFromNamespaceString(namespace),
      strippedPreviousNamespace = stripFromNamespaceString(previousNamespace),
      convertedPreviousNamespace = getParentNamespace(strippedNamespace, strippedPreviousNamespace),
      namespaceComment = (typeof previousNamespace === 'string') ? '* @memberof ' + convertedPreviousNamespace + '\n' : '',
      namespaceFromString = '\n\n/**\n' + '* @namespace ' + strippedNamespace + '\n' + namespaceComment + '*/\n';

    return (typeof namespace === 'string') ? namespaceFromString : '';
  }

  str = 'window.' + appName + ' = {} || ' + appName + ';\n';
  emitter = walk.walk(startPath);

  /**
   * @desc Uppercase letter of each word in string.
   * @function upperCaseMe
   * @inner
   * @param {string} txt String with words to convert to uppercase.
   * @return {string}
   */
  function upperCaseMe(txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  }

  emitter.on('file', function (path, stat, next) {
    if (stat.name[0] !== '.') {
      var filepath = [path, '/', stat.name].join(''),
      noSlash = filepath.replace(/\//g, ' '),
      stripStartPath = noSlash.replace(spacedStartPath, ''),
      stripJs = stripStartPath.replace('.js', ''),
      uppercase = stripJs.replace(/\w\S*/g, upperCaseMe);

      if (uppercase.indexOf(' ') !== -1) {
        uppercase = uppercase.replace(/ /g, '.');
      }

      var pathArr = uppercase.split('.'),
        joined = [],
        joinedStr = '';

      for(var i = 1; i < pathArr.length; i++) {
        joined.push(pathArr[i]);
        joinedStr += appName + '.' + joined.join('.') + ' = {};\n';
      }

      iter.push(joinedStr);
    }

    next();
  });

  emitter.on('end', function () {
    var uniquePaths = [],
      uniqueEntries = [],
      end = '\n',
      finalJoinedString = '';

    for(var i = 0; i < iter.length; i++) {
      if (uniquePaths.indexOf(iter[i]) === -1) {
        uniquePaths.push(iter[i]);
        str += iter[i];
      }
    }

    var strArray = str.split(end);

    for(var i = 0; i < strArray.length; i++) {
      if (uniqueEntries.indexOf(strArray[i]) === -1) {
        uniqueEntries.push(strArray[i]);
        var docblock = (i !== (strArray.length - 1)) ? createDocBloc(
          strArray[i], strArray[i - 1]) : '';
        finalJoinedString += docblock + strArray[i] + end;
      }
    }

    log.write(jsdocFileBlock + finalJoinedString);
  });

});