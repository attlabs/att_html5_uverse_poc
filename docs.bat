@echo off
set JSDOC_BASE_DIR=%CD%\jsdoc-toolkit
java -Djsdoc.dir=%JSDOC_BASE_DIR% -jar %JSDOC_BASE_DIR%\jsrun.jar %JSDOC_BASE_DIR%\app\run.js -c=%JSDOC_BASE_DIR%\conf\jsdoc.conf %*