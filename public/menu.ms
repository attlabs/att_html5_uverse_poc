<div id="menu" class="row-fluid" data-bind="with: response">
    <div class="span12 centering text-center" data-bind="foreach: buttons()">
        <div data-bind="attr: { 'data-loc' : '#/' + view(), 'class' : style()}, css : 'span3 text-center'">
            <div class="details" data-bind="attr: { 'data-loc' : '#/' + view
            () }, text: button_title()" />
        </div>
    </div>
</div>
<script>
$().ready(function(){
    poc.Models.View.Menu.start();
    poc.Models.View.Menu.wireEvents();
});
</script>
