/*! AT&T HTML5 U-verse POC - v0.0.5 - 2013-06-09
* http://morning-thicket-1335.herokuapp.com/#/
* Copyright (c) 2013 Julia Jacobs; Licensed MIT */
'use strict';


/**
* @namespace poc
*/
window.poc = {} || poc;


/**
* @namespace poc.Namespace;
* @memberof poc
*/
poc.Namespace = {};


/**
* @namespace poc.Router;
* @memberof poc
*/
poc.Router = {};


/**
* @namespace poc.Utils;
* @memberof poc
*/
poc.Utils = {};


/**
* @namespace poc.Utils.Detect;
* @memberof poc.Utils
*/
poc.Utils.Detect = {};


/**
* @namespace poc.Utils.Dom_manipulation;
* @memberof poc.Utils
*/
poc.Utils.Dom_manipulation = {};


/**
* @namespace poc.Utils.Strings;
* @memberof poc.Utils
*/
poc.Utils.Strings = {};


/**
* @namespace poc.Utils.Transformlogo;
* @memberof poc.Utils
*/
poc.Utils.Transformlogo = {};


/**
* @namespace poc.Tv;
* @memberof poc
*/
poc.Tv = {};


/**
* @namespace poc.Tv.Main;
* @memberof poc.Tv
*/
poc.Tv.Main = {};


/**
* @namespace poc.Models;
* @memberof poc
*/
poc.Models = {};


/**
* @namespace poc.Models.Factory;
* @memberof poc.Models
*/
poc.Models.Factory = {};


/**
* @namespace poc.Models.View;
* @memberof poc.Models
*/
poc.Models.View = {};


/**
* @namespace poc.Models.View.Menu;
* @memberof poc.Models.View
*/
poc.Models.View.Menu = {};


/**
* @namespace poc.Models.View.Search;
* @memberof poc.Models.View
*/
poc.Models.View.Search = {};


/**
* @namespace poc.Grid;
* @memberof poc
*/
poc.Grid = {};


/**
* @namespace poc.Grid.Details;
* @memberof poc.Grid
*/
poc.Grid.Details = {};


/**
* @namespace poc.Grid.Elements;
* @memberof poc.Grid
*/
poc.Grid.Elements = {};


/**
* @namespace poc.Grid.Layout;
* @memberof poc.Grid
*/
poc.Grid.Layout = {};


/**
* @namespace poc.Data;
* @memberof poc
*/
poc.Data = {};


/**
* @namespace poc.Data.Providers;
* @memberof poc.Data
*/
poc.Data.Providers = {};


/**
* @namespace poc.Data.Providers.Api;
* @memberof poc.Data.Providers
*/
poc.Data.Providers.Api = {};


/**
* @namespace poc.Data.Providers.Names;
* @memberof poc.Data.Providers
*/
poc.Data.Providers.Names = {};


/**
* @namespace poc.Data.Layout;
* @memberof poc.Data
*/
poc.Data.Layout = {};


/**
* @namespace poc.Data.Layout.Buttons;
* @memberof poc.Data.Layout
*/
poc.Data.Layout.Buttons = {};


/**
* @namespace poc.Data.Layout.Presets;
* @memberof poc.Data.Layout
*/
poc.Data.Layout.Presets = {};


/**
* @namespace poc.Data.Api;
* @memberof poc.Data
*/
poc.Data.Api = {};


/**
* @namespace poc.Data.Api.Data;
* @memberof poc.Data.Api
*/
poc.Data.Api.Data = {};


/**
* @namespace poc.Data.Api.Embedly;
* @memberof poc.Data.Api
*/
poc.Data.Api.Embedly = {};

;'use strict';


/**
 * @public
 * @function variables
 * @memberof poc.Utils.Detect
 * @this variables
 * @return {Object}
 */
poc.Utils.Detect.variables = function() {
  var self = this, detectizr;

  Modernizr.Detectizr.detect({
    addAllFeaturesAsClass: true,
    detectDevice: true,
    detectModel: true,
    detectScreen: true,
    detectOS: true,
    detectBrowser: true,
    detectPlugins: true
  });

  self.device = Modernizr.Detectizr.device;
  self.isChrome = (self.device.browser === 'chrome');
  self.isDesktop = (self.device.type === 'desktop');
  self.isChromeDesktop = (self.isChrome && self.isDesktop);
  self.isTablet = (self.device.type === 'tablet');
  self.isTv = (self.device.type === 'tv');
  self.isMobile = (self.device.type === 'mobile');
  self.isLandscape = (self.device.orientation === 'landscape');
  self.isPortrait = (self.device.orientation === 'landscape');
  self.isLandscapeTablet = (self.isTablet && self.isLandscape);
  self.isPortraitTablet = (self.isTablet && self.isPortrait);
  self.upperCase = poc.Utils.Strings.uppercaseMe;

  return self;

};


/**
* @function requiredAppFeatures
* @return {string}
*/
function requiredAppFeatures() {
  var doesNotHaveFeatures = [],
      vars = poc.Utils.Detect.variables(),
      result,

      /** features required by app */
      requiredFeatures = {
        'CSS 3D Transforms': Modernizr.csstransforms3d,
        'CSS Transitions': Modernizr.csstransitions,
        'Hash Change': Modernizr.hashchange,
        'CSS Animations': Modernizr.cssanimations,
        'CSS Opacity': Modernizr.opacity,
        'CSS Font Face': Modernizr.fontface,
        'HTML5 Video API': Modernizr.video,
        'HTML5 Localstorage API': Modernizr.localstorage
      },

      /** describes model and os */
      os = (vars.device.os) ? ' for ' +
      vars.upperCase(vars.device.os) : '',
      model = (vars.device.model) ? ' ' +
      vars.upperCase(vars.device.model) : '',

      /** information about what IS supported */
      notSupported = '<p>Sorry ' + vars.upperCase(vars.device.browser) +
      os + model +
      ' is not supported for this app.</p>',
      SUPPORTED = 'Please use one of these supported devices / browsers:' +
      '<ul><li>Latest version of Chrome</li>' +
      '<li>Samsung SmartTV browser (although the 3D ' +
      'navigation does not work yet)</li>' +
      '<li>iPad in landscape mode (not very well here Im ' +
      'afraid since fullscreen mode is only supported <br />' +
      'when a website is added to the Home Screen)</li></ul>',
      VISITING = '<p>Visiting this site with one of those devices / ' +
      'browsers will give you a link to go to the app and' +
      '<br />directions on how to use it.</p>' +
      '<p>I know its a pain but I hope you wont be disappointed! ' +
      'Thanks for checking this out.',
      message = notSupported + '<p>There are a number of mostly experimental ' +
      'HTML5 CSS3 features that your browser<b/>' +
      'does not support.</p><p>Namley:</p><ul>';

  _.each(requiredFeatures, function(value, key) {
    if (!value && !vars.isTv) {
      doesNotHaveFeatures.push(key);
    }
  });

  if (doesNotHaveFeatures.length > 0) {
    _.each(doesNotHaveFeatures, function(featureName) {
      //noinspection ReuseOfLocalVariableJS
      message = '<li>' + featureName + '</li>';
    });
    result = message + SUPPORTED + VISITING;
  } else {
    result = notSupported + SUPPORTED + VISITING;
  }

  return result;

}


/**
* @function ipadInLandscapeOrientation
* @return {string}
*/
function ipadInLandscapeOrientation() {
  var IPAD = 'This app supports the iPad although you need to ' +
      'be in landscape orientation.  Please<br />' +
      'do so and refresh the website. ' +
      '<p>For best results add the ' +
      'site to the Home Screen.  <br /> ' +
      'This is the only way iOS devices will' +
      'open up a web page in fullscreen mode.  The site layout ' +
      'has been designed for fullscreen mode.',
      VISTING = '<p>Visiting this site in landscape orientation ' +
      'mode will give you a link to go to the app and' +
      '<br />directions on how to use it.</p>' +
      '<p>I know its a pain but I hope you wont be ' +
      'disappointed! Thanks for checking this out.',
      vars = poc.Utils.Detect.variables();

  return (vars.isPortraitTablet) ? IPAD + VISTING : '';
}


/**
* @function requirements
*/
function requirements()
{
  //noinspection GjsLint
  var ALL = 'Welcome!  You are all set to see the app!',
      INSTRUCTIONS = '<b>Instructions:</b><br/> Navigate through 3D video search ' +
      'results grid using a trackpad, trackball or mouse wheel.  ' +
      '<br />For iPad use pinch zoom.',
      GO_TO_APP_LABEL = 'Go to app',
      CODE_AND_DOC_LABEL = 'Code and documentation',
      DOC_URL = 'https://bitbucket.org/attlabs/att_html5_uverse_poc/overview',
      // cacheing jquery selectors
      $hOne = $('h1'),
      $newHeaderTwo = $('<h2 />'),
      $hTwo = $('h2'),
      $newPara = $('<p />'),
      $device_info = $('#device_info'),
      $app = $('#app'),
      $info = $('#info'),
      $window_info = $('#window_info'),
      $no_dice = $('#no_dice'),
      $window = $(window),

      // to get rid of chained call to
      // function code style errors
      appAttr = $app.attr('href', '/app/#/'),
      infoAttr = $info.attr('href', DOC_URL),
      windowInfo = '<dl><dt>Window Height</dt><dd>' + $window.height() +
      '</dd></dl>' +
      '<dl><dt>Window Width</dt><dd>' + $window.width() + '</dd></dl>',
      vars = poc.Utils.Detect.variables(),
      message;

  if (vars.isChromeDesktop || vars.isLandscapeTablet || vars.isTv) {
    $hOne.after(
        $newHeaderTwo.text(ALL)
    );
    $hTwo.after(
        $newPara.html(INSTRUCTIONS));
    $device_info.prettify({highlight: true}, vars.device);
    appAttr.html(GO_TO_APP_LABEL);
    infoAttr.html(CODE_AND_DOC_LABEL);
    $app.on('click', function() {
      screenfull.request();
    });
    $window_info.append(windowInfo);
  } else {
    message = ipadInLandscapeOrientation() + requiredAppFeatures();
    $no_dice.html(message);
  }
}


/**
* @public
* @function init
* @memberof poc.Utils.Detect
*/
poc.Utils.Detect.init = function() {
  requirements();
};
;'use strict';


/**
* @public
* @function init
* @this init
* @memberof poc.Utils.Transformlogo
* @return {Object}
*/
poc.Utils.Transformlogo.init = function() {
  var self = this,
      top,
      left,
      $animImage = $('.animImage'),
      $location = $(location),
      locationAttr = $location.attr('href'),
      vars = poc.Utils.Detect.variables();

  self.hrefArray = locationAttr.split('/');

  //noinspection NestedFunctionJS
  /**
    * @desc Determins what the position of the logo should be
    * when up in corner based on device and window size.
    * @inner
    * @function devicePos
    * @memberof poc.Utils.Transformlogo.func.init
    */
  self.devicePos = function() {

    if (vars.isDesktop || vars.isLandscapeTablet) {
      top = '10%';
      left = '5%';
    }

    if (vars.isPortraitTablet) {
      top = '10%';
      left = '10%';
    }

    if (vars.isMobile) {
      top = '10%';
      left = '10%';
    }
  };
  //noinspection NestedFunctionJS
  /**
    * @desc Determins what the position of the logo should be
    * when up in corner based on device and window size.
    * @inner
    * @function deviceTransit
    * @memberof init
    */
  self.deviceTransit = function() {
    $animImage.transition({ opacity: 1, scale: 30 }, 5000, 'ease');
    $animImage.transition(
        { top: top, left: left, scale: 10 },
        1000,
        'ease',
        function() {
          $location.attr('href', '#/menu');
        });
  };
  //noinspection NestedFunctionJS
  /**
    * @desc Sets end position of animation.
    * @inner
    * @function deviceEndPos
    * @memberof poc.Utils.Transformlogo.func.init
    */
  self.deviceEndPos = function() {
    $animImage.css({
      top: top,
      left: left,
      opacity: 1,
      '-webkit-transform': 'scale(10, 10)'
    });
  };

  return self;
};


/**
* @public
* @function start
* @memberof poc.Utils.Transformlogo
*/
poc.Utils.Transformlogo.start = function() {
  var $window = $(window),
      init = poc.Utils.Transformlogo.init();

  init.devicePos();
  // animation only occurs on home page
  if (init.hrefArray[5] == '') {
    init.deviceTransit();
  } else {
    init.deviceEndPos();
  }

  $window.on('throttledresize', function() {
    init.deviceEndPos();
  });

};

;'use strict';


/**
 * @public
 * @function uppercaseMe
 * @memberof poc.Utils.Strings
 * @param {string} txt
 * @return {string}
 */
poc.Utils.Strings.uppercaseMe = function(txt) {
  var firstLetter = txt.charAt(0),
      remainingText = txt.substr(1);

  return firstLetter.toUpperCase() + remainingText.toLowerCase();
};


/**
 * @public
 * @function urlencode
 * @memberof poc.Utils.Strings
 * @param {string} str
 * @return {string}
 */
poc.Utils.Strings.urlencode = function(str) {
  // to get rid of chained call to
  // function code style errors
  var encode = encodeURIComponent(str),
      exclamation = encode.replace(/!/g, '%21'),
      singleQuote = exclamation.replace(/'/g, '%27'),
      leftParen = singleQuote.replace(/\(/g, '%28'),
      rightParen = leftParen.replace(/\)/g, '%29'),
      backSlash = rightParen.replace(/\*/g, '%2A');

  return backSlash.replace(/%20/g, '+');
};


/**
 * @public
 * @function shortenTitle
 * @memberof poc.Utils.Strings
 * @param {string} txt
 * @return {string}
 */
poc.Utils.Strings.shortenTitle = function(txt) {
  return txt.substr(0, 18) + ' . . .';
};


/**
 * @public
 * @function shortenTitle
 * @memberof poc.Utils.Strings
 * @param {string} txt
 * @return {string}
 */
poc.Utils.Strings.shortenDesc = function(txt) {
  return txt.substr(0, 40) + ' . . .';
};
;/**
 * @author julia_jacobs@labs.att.com (Julia Jacobs)
 * Date: 6/6/13
 * Time: 12:46 PM
 * @file names.js
 * @desc Embedly supported video providers.
 */

'use strict';


/**
 * @public
 * @function all
 * @memberof poc.Data.Providers.Names
 * @return {Array}
 */
poc.Data.Providers.Names.all = [
  'youtube', 'justintv', 'twitchtv', 'ustream', 'qik', 'revision3',
  'dailymotion', 'collegehumor', 'twitvid', 'telly',
  'myspacevideos',
  'metacafe', 'bliptv', 'googlevideo', 'revver', 'yahoovideo', 'viddler',
  'liveleak', 'animoto', 'dotsub',
  'overstream', 'livestream',
  'worldstarhiphop', 'bambuser', 'schooltube', 'bigthink', 'jibjab',
  'xtranormal', 'socialcam', 'dipdive', 'youku',
  'snotr', 'jardenberg',
  'clipfish', 'myvideo', 'vzaar', 'coub', 'streamio', 'vine', 'viddy',
  'whitehouse', 'hulu', 'crackle', 'fancast',
  'funnyordie', 'vimeo', 'ted',
  'nfb', 'thedailyshow', 'yahoomovies', 'colbertnation', 'comedycentral',
  'theonion', 'wordpresstv', 'traileraddict',
  'escapistmagazine', 'trailerspy', 'atom', 'foratv', 'spike', 'gametrailers',
  'koldcasttv', 'mixergy', 'pbsvideo',
  'zapiks', 'digg', 'trutv',
  'nzonscreen', 'wistia', 'hungrynation', 'indymogul', 'channelfrederator',
  'tmiweekly', '99dollarmusicvideos',
  'ultrakawaii',
  'barelypolitical', 'barelydigital', 'threadbanger', 'vodcars', 'confreaks',
  'allthingsd', 'nymag', 'aniboom',
  'grindtv', 'ifoodtv',
  'logotv', 'lonelyplanet', 'streetfire', 'trooptube', 'sciencestage',
  'brightcove', 'wirewax', 'canalplus', 'vevo',
  'pixorial',
  'spreecast', 'showme', 'looplogic', 'onaol', 'videodetective', 'khanacademy',
  'vidyard', 'godtube', 'tangle',
  'mediamatters',
  'clikthrough', 'clipsyndicate', 'espn', 'abcnews', 'washingtonpost', 'boston',
  'facebook', 'cnbc', 'cbsnews',
  'googleplus', 'cnn',
  'cnnedition', 'cnnmoney', 'msnbc', 'globalpost', 'guardian', 'bravotv',
  'nationalgeographic', 'discovery', 'forbes',
  'distrify',
  'foxnews', 'foxbusiness'
];


/**
 * @public
 * @function chosen
 * @memberof poc.Data.Providers.Names
 * @return {Array}
 */
poc.Data.Providers.Names.chosen = [
  'youtube', 'thedailyshow', 'comedycentral', 'spike', 'pbsvideo', 'espn',
  'abcnews', 'washingtonpost',
  'cnnedition', 'msnbc', 'bravotv', 'nationalgeographic', 'discovery', 'forbes',
  'foxnews'
];


/**
 * @public
 * @function enabled
 * @memberof poc.Data.Providers.Names
 * @return {Array}
 */
poc.Data.Providers.Names.enabled = [
  'youtube'
];
;'use strict';


/**
 * @public
 * @function getProviderData
 * @memberof poc.Data.Providers.Api
 * @param {String} providerName Video provider name.
 * @return {Object}
 */
poc.Data.Providers.Api.getProviderData = function(providerName) {
  var type = 'json',
      providerInfo = {
            providers: [
          {
            name: 'youtube',
            url: function(query) {
              return 'https://gdata.youtube.com/feeds/api/videos?' +
                  'v=2' +
                  '&alt=' +
                  type +
                  'c' +
                  '&safeSearch=moderate' +
                  '&start-index=1' +
                  '&max-results=50' +
                  '&hd=true' +
                  '&key=AIzaSyCJ-YABHvUb--o4iofdUu9O8wW5uomRDzc' +
                  '&q=' +
                  query;
            },
            items: function(data) {
              return data.data.items;
            },
            thumbnail: function(item) {
              return item.thumbnail.hqDefault;
            },
            embedly_uri: function(item) {
              return 'http://www.youtube.com/v/' + item.id;
            },
            title: function(item) {
              return item.title;
            },
            description: function(item) {
              return item.description;
            }
          }
            ]
      };

  var selectedProvider = [];

  _.each(providerInfo.providers, function(provider) {
    if (provider.name === providerName) {
      selectedProvider.push(provider);
    }
  });

  return selectedProvider[0];

};
;'use strict';


/** @global */
poc.Data.Api.Embedly.presets = {};


/** @global */
poc.Data.Api.Embedly.key = '3bbe3646427d46b591bcc7eeccbca010';


/**
 * @public
 * @function providerServiceApi
 * @memberof poc.Data.Api.Embedly
 * @return {Object}
 */
poc.Data.Api.Embedly.providerServiceApi = function() {

  var videoProviderData = [],
      names = poc.Data.Providers.Names,
      providerData = {};

  $.ajax({
    url: 'http://api.embed.ly/1/services',
    async: false,
    dataType: 'json',
    success: function(response) {
      _.each(response, function(entry, i) {
        var isChosen = _.indexOf(names.chosen, entry.name) !== -1;
        var isEnabled = _.indexOf(names.enabled, entry.name) !== -1;
        if (entry.type === 'video' && isChosen) {
          entry.enabled = !!isEnabled;
          providerData[i] = entry;
        }
      });

      videoProviderData.push(providerData);
    }
  });

  return videoProviderData;

};


/**
 * @public
 * @function data
 * @memberof poc.Data.Api.Embedly
 * @param {String} uri oEmbed uri parameter for provider.
 * @return {Object}
 */
poc.Data.Api.Embedly.data = function(uri) {
  var API_URL = 'http://api.embed.ly/1/oembed?url=',
      optionStr = [],
      settings = poc.Data.Layout.Presets.settings,
      key = poc.Data.Api.Embedly.key,
      urlencode = poc.Utils.Strings.urlencode,
      options = {
        key: key,
        videosrc: true,
        words: 19,
        maxwidth: settings().embedly.maxwidth,
        maxheight: settings().embedly.maxheight,
        width: settings().embedly.width
      },
      item = [],
      url = API_URL + urlencode(uri) + optionStr.join('');

  _.each(options, function(value, key) {
    var str = '&' + key + '=' + value;
    optionStr.push(str);
  });

  $.ajax({
    url: url,
    async: false,
    dataType: 'json',
    success: function(data) {
      item.push(data);
    }
  });

  return item;
};
;/**
 * @author julia_jacobs@labs.att.com (Julia Jacobs)
 * Date: 6/6/13
 * Time: 1:25 PM
 * @file data.js
 * @desc Builds a data object from all of the video
 * providers for the search and browse pages.
 */

'use strict';


/**
 * @public
 * @function paramTyp
 * @memberof poc.Data.Api.Data
 * @return {String}
 */
poc.Data.Api.Data.paramType = function() {
  var uri = window.location.hash.indexOf;
  return (uri('/search/') === -1) ? 'query' : 'cat';
};


/**
 * @public
 * @function queryData
 * @memberof poc.Data.Api.Data
 * @param {String} query Search query.
 * @param {Object} provider Provider data.
 * @return {Array}
 */
poc.Data.Api.Data.queryData = function(query, provider) {
  var items = [],
      getProviderData = poc.Data.Providers.Api.getProviderData,
      providerName = getProviderData(provider.name),
      providerDataInfoUrl = providerName.url(query);
  $.ajax({
    url: providerDataInfoUrl,
    async: false,
    dataType: 'json',
    success: function(data) {
      var providerDataArrayEntry = getProviderData(provider.name);

      _.each(providerDataArrayEntry.items(data), function(apiItem) {
        var item = {};
        item.icon = provider.icon;
        item.thumbnail = providerDataArrayEntry.thumbnail(apiItem);
        item.embedly_uri = providerDataArrayEntry.embedly_uri(apiItem);
        item.title = providerDataArrayEntry.title(apiItem);
        item.description = providerDataArrayEntry.description(apiItem);
        items.push(item);
      });

    }
  });
  return items;
};


/**
 * @public
 * @function createDataObj
 * @memberof poc.Data.Api.Data
 * @param {String} query Search query.
 * @param {Array} providers Selected providers.
 */
poc.Data.Api.Data.createDataObj = function(query, providers) {

  var REQUIRED_MESSAGE = 'please choose a provider',
      dataEntries = {},
      data,
      queryData = poc.Data.Api.Data.queryData,
      objFactory = poc.Models.Factory.model,
      createGrid = poc.Grid.Layout.createGrid;

  _.each(providers, function(provider, i) {
    dataEntries[i] = queryData(query, provider);
  });

  data = objFactory(dataEntries);

  if (providers.length > 0) {
    createGrid(data);
  } else {
    alert(REQUIRED_MESSAGE);
  }

};
;;/**
 * @author julia_jacobs@labs.att.com (Julia Jacobs)
 * Date: 6/3/13
 * Time: 11:27 AM
 * @file router.js
 * @desc Sammy SPA router using {@link http://mustache.github.io/ mustache}
 * templates.
 */

/*globals Sammy, $ */

'use strict';


/**
 * @public
 * @function init
 * @memberof poc.Router
 * @todo Fix to swap out dom instead of
 * having to remove and recreate it on every page
 */
poc.Router.init = function() {

  var app = Sammy('#container', function() {
    var CONTAINER_FIRST_CHILD_SELECTOR = '#container > div:first-child',
        $main = $('#main'),
        $menu = $('#menu'),
        $search = $('#search'),
        $container = $('#container'),
        $index = $('#index'),
        $div = $('<div />'),
        $divWithId = $div.attr('id', 'main'),

        // private animation / effects functions
        fadePageIn = function() {
          var elemObj = $(CONTAINER_FIRST_CHILD_SELECTOR);
          elemObj.hide();
          elemObj.fadeIn(3000);
        };

    this.use(Sammy.Mustache);
    this.get('#/', function() {
      localStorage.clear();
      this.partial('/index.ms', function(html) {
        $main.html(html);
      });
    });
    this.get('#/search', function() {
      $main.remove();
      $container.append($divWithId);
      this.partial('/search.ms', function(html) {
        $main.html(html);
      });
    });
    this.get('#/search/:query', function() {
      $search.remove();
      $container.append($divWithId);
      this.partial('/search.ms', function(html) {
        $main.html(html);
      });
    });
    this.get('#/settings', function() {
      $menu.remove();
      $container.append($divWithId);
      this.partial('/settings.ms', function(html) {
        $main.html(html);
      });
    });
    this.get('#/social', function() {
      $menu.remove();
      $container.append($divWithId);
      this.partial('/social.ms', function(html) {
        $main.html(html);
      });
    });
    this.get('#/library', function() {
      $menu.remove();
      $container.append($divWithId);
      this.partial('/library.ms', function(html) {
        $main.html(html);
      });
    });
    this.get('#/menu', function() {
      $index.remove();
      $container.append($divWithId);
      this.partial('/menu.ms', function(html) {
        $main.html(html);
        fadePageIn();
      });
    });

  });

  app.run('/app/#/');

};

;/**
 * @author julia_jacobs@labs.att.com (Julia Jacobs)
 * Date: 6/6/13
 * Time: 12:53 PM
 * @file presets.js
 * @desc Responsiver design grid element layout presets.
 */

'use strict';


/**
 * @public
 * @function settings
 * @memberof poc.Data.Layout.Presets
 * @return {Object}
 */
poc.Data.Layout.Presets.settings = function() {
  var layout,
      type = Modernizr.Detectizr.device.type,

      // media query constants
      PC_DESKTOP = 'screen and (max-width: 1366px)',
      MACBOOK_PRO_DESKTOP = 'screen and (max-width: 1440px)',
      PROJECTOR = 'screen and (max-width: 1024px)',
      BIG_MONITOR = 'screen and (max-width: 1920px)',
      MOBILE_LANDSCAPE = 'screen and (max-device-width: 480px) ' +
                       'and (orientation: landscape)',
      MOBILE_PORTRAIT = 'screen and (max-device-width: 320px) ' +
                      'and (orientation: portrait)',
      TABLET_LANDSCAPE = '(max-device-width: 1024px) ' +
                       'and (orientation: landscape)',
      TABLET_PORTRAIT = '(max-device-width: 768px) and (orientation: portrait)';
  // Desktops
  if (type === 'desktop') {
    // pc desktop
    if (Modernizr.mq(PC_DESKTOP)) {
      layout = {
        'objAcross': 5,
        'objDown': 3,
        'spaceBtwObj': 600,
        'spaceBtwLayers': 2000,
        'cameraZ': 5400,
        'cameraX': -25,
        'cameraY': -200,
        'embedly' :
            {
              'maxwidth': 500,
              'maxheight': 500,
              'width': 200
            },
        'social' :
            {
              'offsetLocation': 0,
              'offsetAlign': -80
            }
      };
    }
    // projector
    if (Modernizr.mq(PROJECTOR)) {
      layout = {
        'objAcross': 5,
        'objDown': 3,
        'spaceBtwObj': 600,
        'spaceBtwLayers': 2000,
        'cameraZ': 5400,
        'cameraX': -25,
        'cameraY': -200,
        'embedly' :
            {
              'maxwidth': 500,
              'maxheight': 500,
              'width': 500
            },
        'social' :
            {
              'offsetLocation': 0,
              'offsetAlign': -80
            }
      };
    }
    // big monitor
    if (Modernizr.mq(BIG_MONITOR)) {
      layout = {
        'objAcross': 5,
        'objDown': 3,
        'spaceBtwObj': 600,
        'spaceBtwLayers': 2000,
        'cameraZ': 5400,
        'cameraX': -25,
        'cameraY': -200,
        'embedly' :
            {
              'maxwidth': 500,
              'maxheight': 500,
              'width': 200
            },
        'social' :
            {
              'offsetLocation': 0,
              'offsetAlign': -80
            }
      };
    }
    // mac desktop
    if (Modernizr.mq(MACBOOK_PRO_DESKTOP)) {
      layout = {
        'objAcross': 5,
        'objDown': 3,
        'spaceBtwObj': 600,
        'spaceBtwLayers': 2000,
        'cameraZ': 5400,
        'cameraX': -25,
        'cameraY': -200,
        'embedly' :
            {
              'maxwidth': 500,
              'maxheight': 800,
              'width': 500
            },
        'social' :
            {
              'offsetLocation': 0,
              'offsetAlign': -80
            }
      };
    }
  }
  // iPhone4
  if (type === 'mobile') {
    // Landscape
    if (Modernizr.mq(MOBILE_LANDSCAPE)) {
      layout = {
        'objAcross': 4,
        'objDown': 2,
        'spaceBtwObj': 600,
        'spaceBtwLayers': 2000,
        'cameraZ': 5400,
        'cameraX': -25,
        'cameraY': -200,
        'embedly' :
            {
              'maxwidth': 500,
              'maxheight': 500,
              'width': 200
            },
        'social' :
            {
              'offsetLocation': 0,
              'offsetAlign': -80
            }
      };
    }
    // Portrait
    if (Modernizr.mq(MOBILE_PORTRAIT)) {
      layout = {
        'objAcross': 5,
        'objDown': 3,
        'spaceBtwObj': 600,
        'spaceBtwLayers': 2000,
        'cameraZ': 5400,
        'cameraX': -25,
        'cameraY': -200,
        'embedly' :
            {
              'maxwidth': 500,
              'maxheight': 500,
              'width': 200
            },
        'social' :
            {
              'offsetLocation': 0,
              'offsetAlign': -80
            }
      };
    }
  }
  // iPad 2
  if (type === 'tablet') {
    // Landscape
    if (Modernizr.mq(TABLET_LANDSCAPE)) {
      layout = {
        'objAcross': 3,
        'objDown': 2,
        'spaceBtwObj': 600,
        'spaceBtwLayers': 2000,
        'cameraZ': 15093,
        'cameraX': -20,
        'cameraY': -100,
        'embedly' :
            {
              'maxwidth': 500,
              'maxheight': 500,
              'width': 500
            },
        'social' :
            {
              'offsetLocation': 0,
              'offsetAlign': -80
            }
      };
    }
    // Portrait
    if (Modernizr.mq(TABLET_PORTRAIT)) {
      layout = {
        'objAcross': 3,
        'objDown': 2,
        'spaceBtwObj': 600,
        'spaceBtwLayers': 2000,
        'cameraZ': 5400,
        'cameraX': -25,
        'cameraY': -200,
        'embedly' :
            {
              'maxwidth': 500,
              'maxheight': 500,
              'width': 200
            },
        'social' :
            {
              'offsetLocation': 0,
              'offsetAlign': -80
            }
      };
    }
  }

  // TV
  if (type === 'tv') {
    layout = {
      'objAcross': 3,
      'objDown': 2,
      'spaceBtwObj': 600,
      'spaceBtwLayers': 2000,
      'cameraZ': 5400,
      'cameraX': -25,
      'cameraY': -200,
      'embedly' :
          {
            'maxwidth': 500,
            'maxheight': 500,
            'width': 200
          },
      'social' :
          {
            'offsetLocation': 0,
            'offsetAlign': -80
          }
    };
  }

  return layout;
};
;'use strict';


/**
 * @public
 * @function runGrid
 * @memberof poc.Grid.Layout
 */
poc.Grid.Layout.runGrid = function() {
  poc.Grid.Layout.init();
  poc.Grid.Layout.animate();
};


/** @global */
poc.Grid.Layout.orgObjectsPosition = [];


/** @global */
poc.Grid.Layout.sceneElements = {};


/** @global */
poc.Grid.Layout.totalObj = 50;


/**
 * @public
 * @function init
 * @memberof poc.Grid.Layout
 */
poc.Grid.Layout.init = function() {
  var camera,
      scene,
      renderer,
      controls,
      settings = poc.Data.Layout.Presets.settings(),

      // camera options
      fov = 75,
      aspect = window.innerWidth / window.innerHeight,
      near = 0.1,
      far = 5000,
      videoContainer = document.getElementById('video_container');

  camera = new THREE.PerspectiveCamera(fov, aspect, near, far);

  camera.position.z = settings.cameraZ;
  camera.position.x = settings.cameraX;
  camera.position.y = settings.cameraY;

  scene = new THREE.Scene();

  // CSS3D renderer
  renderer = new THREE.CSS3DRenderer();
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.domElement.style.position = 'absolute';
  videoContainer.appendChild(renderer.domElement);

  // TrackballControls also handles gestures
  controls = new THREE.TrackballControls(camera);

  controls.rotateSpeed = 1.0;
  controls.zoomSpeed = 1.2;
  controls.panSpeed = 0.8;

  controls.noZoom = false;
  controls.noPan = false;

  controls.staticMoving = true;
  controls.dynamicDampingFactor = 0.3;

  controls.keys = [65, 83, 68];

  controls.addEventListener('change', poc.Grid.Layout.render);

  poc.Grid.Layout.sceneElements = {
    camera: camera,
    scene: scene,
    renderer: renderer,
    controls: controls
  };
};


/**
 * @public
 * @function render
 * @memberof poc.Grid.Layout
 */
poc.Grid.Layout.render = function() {
  var elements = poc.Grid.Layout.sceneElements,
      scene = elements.scene,
      render = elements.renderer.render,
      camera = elements.camera;

  render(scene, camera);
};


/**
 * @public
 * @function animate
 * @memberof poc.Grid.Layout
 */
poc.Grid.Layout.animate = function() {
  var layout = poc.Grid.Layout,
      controls = layout.sceneElements.controls;

  requestAnimationFrame(layout.animate);
  TWEEN.update();
  controls.update();
};


/**
 * @public
 * @function createGrid
 * @memberof poc.Grid.Layout
 * @param {Object} data Video provider data.
 * @this createGrid
 */
poc.Grid.Layout.createGrid = function(data) {
  var grid = [],
      scene = poc.Grid.Layout.sceneElements.scene,
      cameraElement = poc.Grid.Layout.sceneElements.cameraElement,
      dynPos = poc.Data.Layout.Presets.settings(),
      totalObj = poc.Grid.Layout.totalObj,
      down = dynPos.objDown,
      across = dynPos.objAcross,
      fullGrid = (down * across),
      totalGridObj = (Math.floor(totalObj / fullGrid) * fullGrid),
      objects = poc.Grid.Elements.objects,
      space = dynPos.spaceBtwObj,
      layerSpace = dynPos.spaceBtwLayers,
      eq = ((Math.round(across / 2) - 1) * space),
      position = poc.Grid.Layout.orgObjectsPosition,
      duration = 2000;

  poc.Grid.Elements.elements(data);



  //noinspection NestedFunctionJS
  function tweenObj(object, i) {
    var delay;
    if (i < totalGridObj) {
      delay = Math.random() * 1000;

      //noinspection ChainedFunctionCallJS
      new TWEEN.Tween(object.position)
        .to({ y: -3000 }, 1000)
        .delay(delay)
        .easing(TWEEN.Easing.Exponential.In)
        .start();

      //noinspection ChainedFunctionCallJS
      new TWEEN.Tween(object)
        .to({}, 2000)
        .delay(delay)
        .onComplete(function() {
            var objs = this.objects,
                        index = objs.indexOf(this);
            scene.remove(this);
            cameraElement.removeChild(this.element);
            objs.splice(index, 1);
          })
        .start();
    }
  }

  _.each(objects, tweenObj);

  //noinspection NestedFunctionJS
  function objPos(object, i) {
    var newObject = new THREE.Object3D();

    if (i < totalObj) {
      {
        object.position.x = Math.random() * 4000 - 2000;
        object.position.y = Math.random() * 4000 - 2000;
        object.position.z = Math.random() * 4000 - 2000;

        scene.add(object);

        newObject.position.x = ((i % across) * space) - eq;
        newObject.position.y = (-(Math.floor(i / across) % down) * space) + eq;
        newObject.position.z = (Math.floor(i / (down * across))) * layerSpace;

        grid.push(newObject);
      }
    }
  }

  _.each(objects, objPos);

  TWEEN.removeAll();

  //noinspection NestedFunctionJS
  function endTweenObj(object, i) {
    var target = grid[i],
        tp = target.position,
        tr = target.rotation,
        posEq = Math.random() * duration + duration;

    if (i < totalObj) {
      //noinspection ChainedFunctionCallJS
      new TWEEN.Tween(object.position)
        .to({ x: tp.x, y: tp.y, z: tp.z }, posEq)
        .easing(TWEEN.Easing.Exponential.InOut)
        .start();

      //noinspection ChainedFunctionCallJS
      new TWEEN.Tween(object.rotation)
        .to({ x: tr.x, y: tr.y, z: tr.z }, posEq)
        .easing(TWEEN.Easing.Exponential.InOut)
        .start();

      position.push(object.position);
    }
  }

  _.each(objects, endTweenObj);

  //noinspection ChainedFunctionCallJS
  new TWEEN.Tween(this)
    .to({}, duration * 2)
    .onUpdate(poc.Grid.Layout.render)
    .start();

};
;'use strict';


/**
 * @public
 * @function panel
 * @memberof poc.Grid.Details
 * @param {Object} item
 */
poc.Grid.Details.panel = function(item) {
  // caching jquery selectors
  // and fixing chaining errors
  var DETAILS_STYLES = 'video_detail_content detailspaceygreen',
      $newDiv = $('<div/>'),
      $newPara = $('<p />'),
      $newLink = $('<a />'),
      $newIcon = $('<i />'),
      $newIframe = $('iframe'),
      $video_info_container = $('#video_info_container'),
      $video_detail_content = $('.video_detail_content.detailspaceygreen'),
      $item = $(item[0].html),
      player = $item.css({
        'z-index': '800000'
      }),
      social = $newDiv.attr('id', 'social-share'),
      title = $newPara.attr('html', item[0].title),
      description = $newPara.attr('html', item[0].description),
      authorLink = $newLink.attr({
        href: item[0].author_url,
        html: item[0].author_name
      }),
      $socialShare = $('#social-share'),
      settings = poc.Data.Layout.Presets.settings,
      authorLinkParent = $newPara.append(authorLink),
      videoDetailsCss = $newDiv.addClass(DETAILS_STYLES),
      videoDetailsStyles = videoDetailsCss.css({width: item[0].width - 32}),
      videoDetails = videoDetailsStyles.append(
      title,
      description,
      authorLinkParent
      ),
      $opacity_background = $('#opacity_background'),
      opacity_background = $opacity_background.css({
        'background-color': 'black',
        opacity: '0.5',
        height: '100%',
        width: '100%'
      }),
      $main_vid_info = $('#main_vid_info'),
      main_vid_info = $main_vid_info.css({
        position: 'absolute'
      }),
      $searchIcon = $('.icon-search'),
      $homeIcon = $('.icon-home'),
      video_info_containerStyles = $video_info_container.css({
        position: 'absolute',
        left: Math.floor((window.innerWidth / 2) - (item[0].width / 2))
      }),
      linkSrc = $newLink.attr('src', '#'),
      linkStyles = linkSrc.css({
        position: 'absolute',
        bottom: 0,
        right: 0
      }),
      icon_posStyles = $newIcon.css({
        'float': 'right',
        'cursor': 'pointer'
      }),
      icon_pos = icon_posStyles.addClass('icon-remove-sign'),
      link = linkStyles.append(icon_pos),
      clickEventVideoContainerLink = link.on('click',
      function() {
        $video_info_container.fadeOut();
        $video_info_container.empty();
        $video_info_container.removeAttr('style');
        main_vid_info.removeAttr('style');
        $searchIcon.show();
        $homeIcon.show();
      }
      ),
      video_info_container = video_info_containerStyles.append(
      clickEventVideoContainerLink,
      social,
      player,
      videoDetails
      );

  video_info_container.fadeIn();

  $socialShare.ready(function() {
    $socialShare.dcSocialShare({
      buttons: 'twitter,facebook,plusone,email',
      size: 'vertical',
      txtEmail: 'Email',
      twitterId: 'AttUser1',
      email: 'julia_jacobs@labs.att.com',
      url: $newIframe.attr('src'),
      title: item[0].title,
      description: item[0].description,
      classWrapper: 'dcssb-float',
      classContent: 'dcssb-content',
      location: 'top',
      align: 'right',
      offsetLocation: settings().social.offsetLocation,
      offsetAlign: settings().social.offsetAlign,
      center: 0,
      speedFloat: 1500,
      speed: 600,
      floater: true,
      autoClose: false,
      loadOpen: true,
      easing: 'easeOutQuint',
      classOpen: 'dc-open',
      classClose: 'dc-close',
      classToggle: 'dc-toggle'
    });
  });

  $video_detail_content.ready(function() {
    $video_info_container.css({
      height: $video_detail_content.height() + item[0].height + 32,
      top: Math.floor(
          (window.innerHeight) / 2 -
          (($video_detail_content.height() + item[0].height + 32) / 2)
      )
    });
  });
};
;'use strict';


/**
 * @public
 * @function objects
 * @memberof poc.Grid.Elements
 * @return {Array}
 */
poc.Grid.Elements.objects = [];


/**
 * @public
 * @function playing
 * @memberof poc.Grid.Elements
 * @return {Boolean}
 */
poc.Grid.Elements.playing = false;


/**
 * @public
 * @function elements
 * @memberof poc.Grid.Elements
 * @param {Object} data Data from provider APIs.
 */
poc.Grid.Elements.elements = function(data) {
  var POSTER_IMG_CLASS = 'elposter smaller',
      EL_CLASS = 'video_element smaller',
      $newImg = $('<img/>'),
      $newPara = $('<p/>'),
      $newSpan = $('<span/>'),
      $newDiv = $('<div/>'),
      $window = $(window),
      embedly = poc.Data.Api.Embedly,
      shortenTitle = poc.Utils.Strings.shortenTitle,
      sceneElements = poc.Grid.Layout.sceneElements,
      camera = sceneElements.camera,
      $searchIcon = $('.icon-search'),
      $homeIcon = $('.icon-home'),
      $closeIcon = $('.icon-remove-sign'),
      object,
      renderer = sceneElements.renderer;

  _.each(data[0], function(item, i) {

    var $posterImg = $newImg.attr({
      id: 'element_poster_' + i,
      src: item.thumbnail
    }),
        $iconImg = $newImg.attr({
          src: item.icon,
          align: 'center'
        }),
        $infoContent = $newSpan.html(shortenTitle(item.title)),
        $info = $newPara.append(
            $iconImg.addClass('element_icon'),
            $infoContent.addClass('greenInfo')
        ),
        $newElDivAttr = $newDiv.attr({ id: 'element_id_' + i }),
        elDiv = $newElDivAttr.addClass(EL_CLASS),

        // poster image for video element
        elPoster = $posterImg.addClass(POSTER_IMG_CLASS),

        // details
        info = $info.addClass('elementspaceygreen'),

        element_darker_overlay = $newDiv.css({
              'background-color': 'black',
              opacity: '0.6',
              position: 'absolute',
              cursor: 'pointer',
              height: '471px',
              width: '480px',
              'z-index': 999
            }),
        elDivDarker = elDiv.attr({
          id: 'element_id_' + i
        }),
        // parent element
        elementParentClass = elDivDarker.addClass(EL_CLASS),
        elementParent = elementParentClass.append(elPoster, info),

        element_containerClass = $newDiv.addClass('smaller'),
        element_container = element_containerClass.append(
        element_darker_overlay,
        elementParent
        );

    element_container.on('mouseleave', function() {
      element_darker_overlay.css({
        opacity: '0.6'
      });
    });

    element_container.on('mouseenter', function() {
      element_darker_overlay.css({
        opacity: '0'
      });
    });

    // resize everything when browser window size changes
    $window.resize(function() {
      camera.aspect = $window.width() / $window.height();
      camera.updateProjectionMatrix();

      renderer.setSize($window.width(), $window.height());
    });

    object = new THREE.CSS3DObject($(element_container)[0]);

    poc.Grid.Elements.objects.push(object);

    element_container.properties = { embedly_uri: item.embedly_uri };

    // Create embedly populated video detail panel
    element_container.on('click', function() {
      var item;
      $searchIcon.hide();
      $homeIcon.hide();
      $closeIcon.show();
      item = embedly.data(element_container.properties.embedly_uri);
      poc.Grid.Details.panel(item);
    });

    /* TODO - for tv app
    var tvInit = poc.Tv.Main.init,
      $newLink =  $('<a />'),
      $newLinkSrc = $newLink.attr({
        href: 'javascript:void(0);'
        }),
      tvLink = $newLinkSrc.addClass('tvfocus');

      element_container.on('keydown', getEmbedlyData);

      function getEmbedlyData() {
        tvInit.keyDown(element_container.properties.embedly_uri);
      }

      if (Modernizr.Detectizr.device.type === 'tv') {
        tvLink.append(element_container);
      }
  */

  });
};
;'use strict';


/**
 * @public
 * @function menu
 * @memberof poc.Data.Layout.Buttons
 * @return {Object}
 */
poc.Data.Layout.Buttons.menu = {
  'buttons': [
    {
      'style': 'icon-search',
      'button_title': 'Search',
      'view': 'search'
    },
    {
      'style': 'icon-th',
      'button_title': 'Library',
      'view': 'library'
    },
    {
      'style': 'icon-comments-alt',
      'button_title': 'Social',
      'view': 'social'
    },
    {
      'style': 'icon-cog',
      'button_title': 'Settings',
      'view': 'settings'
    }
  ]
};
;; /**
 * @author julia_jacobs@labs.att.com (Julia Jacobs)
 * Date: 6/3/13
 * Time: 12:56 PM
 * @file menu.js
 * @desc  Knockout.js Menu view model.
 */

'use strict';


/**
 * @public
 * @function viewModel
 * @this viewModel
 * @memberof  poc.Models.View.Menu
 * @return {Object}
 */
poc.Models.View.Menu.viewModel = function() {
  var self = this;
  var data = poc.Data.Layout.Buttons.menu;
  self.response = ko.mapping.fromJS(data);
  return self;
};


/**
 * @public
 * @function start
 * @memberof  poc.Models.View.Menu
 */
poc.Models.View.Menu.start = function() {
  var viewModel = poc.Models.View.Menu.viewModel();
  poc.Models.View.Menu.wireEvents();
  ko.applyBindings(viewModel, document.getElementById('menu'));
};


/**
 * @public
 * @function wireEvents
 * @memberof  poc.Models.View.Menu
 */
poc.Models.View.Menu.wireEvents = function() {
  var $document = $(document),
      $location = $(location),
      $icon = $('div[class*="icon-"]'),
      $label = $('.details');

  $document.on('click', function(e) {
    var target = e.target,
        $target = $(target),
        loc = $target.data('loc');

    if ($icon.is(target)) {
      $location.attr('href', loc);
    }

    if ($label.is(target)) {
      $location.attr('href', loc);
    }
  });
};

;'use strict';


/**
 * @public
 * @function start
 * @memberof poc.Models.View.Search
 */
poc.Models.View.Search.start = function()
    {
  var IMAGE_CHECKED = '../img/smallCheckMark.png',
      IMAGE_WIDTH = '33px',
      IMAGE_HEIGHT = '33px',
      IMAGE_CLASS = 'white rounded-corners shadow',
      IMAGE_PADDING = '4px 0 0 3px',

      // cacheing jquery selectors
      $search = $('#search'),
      $providers = $('#providers'),
      $query = $('#query'),
      $location = $(location),
      $locationHref = $location.attr('href'),
      $window = $(window),
      $newDiv = $('<div />'),
      $newInput = $('<input/>'),
      $newImage = $('<img />'),
      $search_box = $('#search_box'),
      $search_button = $('#search_button'),

      query = $.totalStorage('query'),

      // Check boxes for providers
      providers = [],
      hrefArray = $locationHref.split('/');

  /**
  * @function viewModel
  * @todo incorporate video provider data into view model
  */
  var viewModel = function() {};

  var providerObj = {};

  /**
  * @function inputIsChecked
  * @param {Boolean} isChecked
  */
  var inputIsChecked = function(isChecked) {
    if (isChecked) {
      //save provider name and object in localstorage
      $.totalStorage(
          providerObj.name,
          {
            name: providerObj.name,
            icon: providerObj.favicon
          }
      );
    } else {
      localStorage.clear();
    }
  };

  // to get rid of chained call to
  // function code style errors
  var $newInputId = $newInput.attr('id', providerObj.name),
      $newInputType = $newInputId.attr('type', 'checkbox'),
      $newImageFavicon = $newImage.attr('src', providerObj.favicon),
      imageWrapper = $newDiv.css({
        cursor: 'pointer',
        position: 'relative',
        width: IMAGE_WIDTH,
        height: IMAGE_HEIGHT,
        padding: IMAGE_PADDING
      }),
      disabledImageElem = $newImageFavicon.css({
        width: IMAGE_WIDTH,
        height: IMAGE_HEIGHT
      });

  /**
      * @function fadeSearchBoxOnEnter
      * @param {Object} event
      */
  var fadeSearchBoxOnEnter = function(event) {
    if (event.keyCode === 13) {
      $search_box.fadeOut({
        done: function() {
          // add query param to url
          if ($query.val() != '') {
            window.location.hash = '/search/' + $query.val();
          }
        }
      });
    }
  };

  /**
      * @function wireEvents
      * @this $query
      */
  var wireEvents = function() {
    var createProviderElements;
    var i,
        providerServiceApi = poc.Data.Api.Embedly.providerServiceApi,
            getItem = localStorage.getItem,
            getKey = localStorage.key;

    $search.css({
      height: $window.height()
    });

    createProviderElements = function(provider, i) {
      if (provider.enabled) {
        inputElem.overlayImageCheck({
          image: provider.favicon,
          imageChecked: IMAGE_CHECKED,
          overlayCheckedImage: true,
          width: IMAGE_WIDTH,
          height: IMAGE_HEIGHT,
          wasChecked: $.totalStorage(provider.name) ? true : false,
          afterCheck: inputIsChecked(isChecked)
        });

        container.append(inputElem);

      } else {
        container.addClass('disabled');
        container.append(imageWrapper);
        imageWrapper.append(disabledImageElem);
      }
    };

    _.each(providerServiceApi()[0], function(provider, index) {
      createProviderElements(provider, index);
    });

    $providers.append(container);

    // set the query value to an empty string so
    // when the page first loads, the query
    // value is an empty string
    if (typeof hrefArray[6] == 'undefined') {
      $query.val('');
      $.totalStorage.deleteItem('query');
    } else {
      $search_box.hide();
      $.totalStorage('query', hrefArray[5]);
      $.totalStorage.deleteItem('query');
      $query.val(query);

      // when refreshing the page, get providers from localstorage
      if (localStorage.length > 0) {
        for (i = 0; i < localStorage.length; i++) {
          if (getItem(getKey(i)) !== null) {
            providers.push(JSON.parse(getItem(getKey(i))));
          }
        }
      }


      poc.Grid.Layout.runGrid();
      poc.Data.Api.Data.createDataObj(query, providers);
    }

    // Events to get param
    $query.on('keyup', this, fadeSearchBoxOnEnter(event));

    // execute search function when search button is pressed
    $search_button.on('click', function() {
      if ($query.val() != '') {
        window.location.hash = '/search/' + $query.val();
      }
    });
  };

  wireEvents();
  ko.applyBindings(viewModel, document.getElementById('search'));

};
