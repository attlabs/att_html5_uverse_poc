/**
 * video_api_data.js
 *
 * User: jjacobs
 * Date: 4/15/13
 * Time: 4:22 PM
 *
 * Builds a data object from all of the video
 * providers for the search and browse pages
 */

/**
 * @author julia_jacobs@labs.att.com (Julia Jacobs)
 * Date: 6/6/13
 * Time: 1:25 PM
 * @file data.js
 * @desc Builds a data object from all of the video
 * providers for the search and browse pages.
 */

// 'use strict';


/**
 * @public
 * @function paramTyp
 * @memberof poc.Data.Api.Data
 * @return {String}
 */
poc.Data.Api.Data.paramType = function() {
  var uri = window.location.hash.indexOf;
  return (uri('/search/') === -1) ? 'query' : 'cat';
};


/**
 * @public
 * @function queryData
 * @memberof poc.Data.Api.Data
 * @param {String} query Search query.
 * @param {Object} provider Provider data.
 * @return {Array}
 */
poc.Data.Api.Data.queryData = function(query, provider) {
  var items = [],
      getProviderData = poc.Data.Providers.Api.getProviderData,
      providerName = getProviderData(provider.name),
      providerDataInfoUrl = providerName.url(query);
  $.ajax({
    url: providerDataInfoUrl,
    async: false,
    dataType: 'json',
    success: function(data) {
      var providerDataArrayEntry = getProviderData(provider.name);

      _.each(providerDataArrayEntry.items(data), function(apiItem) {
        var item = {};
        item.icon = provider.icon;
        item.thumbnail = providerDataArrayEntry.thumbnail(apiItem);
        item.embedly_uri = providerDataArrayEntry.embedly_uri(apiItem);
        item.title = providerDataArrayEntry.title(apiItem);
        item.description = providerDataArrayEntry.description(apiItem);
        items.push(item);
      });

    }
  });
  return items;
};


/**
 * @public
 * @function createDataObj
 * @memberof poc.Data.Api.Data
 * @param {String} query Search query.
 * @param {Array} providers Selected providers.
 */
poc.Data.Api.Data.createDataObj = function(query, providers) {

  var REQUIRED_MESSAGE = 'please choose a provider',
      data = {},
      queryData = poc.Data.Api.Data.queryData,
      createGrid = poc.Grid.Layout.createGrid;

  _.each(providers, function(provider, i) {
    data[i] = queryData(query, provider);
  });

  if (providers.length > 0) {
    createGrid(data);
  } else {
    alert(REQUIRED_MESSAGE);
  }

};
