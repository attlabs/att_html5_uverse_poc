/**
 * @author julia_jacobs@labs.att.com (Julia Jacobs)
 * Date: 6/6/13
 * Time: 1:16 PM
 * @file embedly.js
 * @desc Embedly service api - used to get names of providers and favicon.
 */

// 'use strict';


/** @global */
poc.Data.Api.Embedly.presets = {};


/** @global */
poc.Data.Api.Embedly.key = '3bbe3646427d46b591bcc7eeccbca010';


/**
 * @public
 * @function providerServiceApi
 * @memberof poc.Data.Api.Embedly
 * @return {Object}
 */
poc.Data.Api.Embedly.providerServiceApi = function() {

  var videoProviderData = [],
      names = poc.Data.Providers.Names,
      providerData = {};

  $.ajax({
    url: 'http://api.embed.ly/1/services',
    async: false,
    dataType: 'json',
    success: function(response) {
      _.each(response, function(entry, i) {
        var isChosen = _.indexOf(names.chosen, entry.name) !== -1;
        var isEnabled = _.indexOf(names.enabled, entry.name) !== -1;
        if (entry.type === 'video' && isChosen) {
          entry.enabled = !!isEnabled;
          providerData[i] = entry;
        }
      });

      videoProviderData.push(providerData);
    }
  });

  return videoProviderData;

};


/**
 * @public
 * @function data
 * @memberof poc.Data.Api.Embedly
 * @param {String} uri oEmbed uri parameter for provider.
 * @return {Object}
 */
poc.Data.Api.Embedly.data = function(uri) {
  var API_URL = 'http://api.embed.ly/1/oembed?url=',
      optionStr = [],
      settings = poc.Data.Layout.Presets.settings,
      key = poc.Data.Api.Embedly.key,
      urlencode = poc.Utils.Strings.urlencode,
      options = {
        key: key,
        videosrc: true,
        words: 19,
        maxwidth: settings().embedly.maxwidth,
        maxheight: settings().embedly.maxheight,
        width: settings().embedly.width
      },
      item = [],
      url = API_URL + urlencode(uri) + optionStr.join('');

  _.each(options, function(value, key) {
    var str = '&' + key + '=' + value;
    optionStr.push(str);
  });

  $.ajax({
    url: url,
    async: false,
    dataType: 'json',
    success: function(data) {
      item.push(data);
    }
  });

  return item;
};
