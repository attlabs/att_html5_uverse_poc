/**
 * @author julia_jacobs@labs.att.com (Julia Jacobs)
 * Date: 6/6/13
 * Time: 12:53 PM
 * @file presets.js
 * @desc Responsiver design grid element layout presets.
 */

// 'use strict';


/**
 * @public
 * @function settings
 * @memberof poc.Data.Layout.Presets
 * @return {Object}
 */
poc.Data.Layout.Presets.settings = function() {
  var layout,
      type = Modernizr.Detectizr.device.type,

      // media query constants
      PC_DESKTOP = 'screen and (max-width: 1366px)',
      MACBOOK_PRO_DESKTOP = 'screen and (max-width: 1440px)',
      PROJECTOR = 'screen and (max-width: 1024px)',
      BIG_MONITOR = 'screen and (max-width: 1920px)',
      MOBILE_LANDSCAPE = 'screen and (max-device-width: 480px) ' +
                       'and (orientation: landscape)',
      MOBILE_PORTRAIT = 'screen and (max-device-width: 320px) ' +
                      'and (orientation: portrait)',
      TABLET_LANDSCAPE = '(max-device-width: 1024px) ' +
                       'and (orientation: landscape)',
      TABLET_PORTRAIT = '(max-device-width: 768px) and (orientation: portrait)';
  // Desktops
  if (type === 'desktop') {
    // pc desktop
    if (Modernizr.mq(PC_DESKTOP)) {
      console.log("PC_DESKTOP");
      layout = {
        'objAcross': 5,
        'objDown': 3,
        'spaceBtwObj': 600,
        'spaceBtwLayers': 2000,
        'cameraZ': 5400,
        'cameraX': 0,
        'cameraY': 0,
        'embedly' :
            {
              'maxwidth': 500,
              'maxheight': 500,
              'width': 200
            },
        'social' :
            {
              'offsetLocation': 0,
              'offsetAlign': -80
            }
      };
    }
    // projector
    if (Modernizr.mq(PROJECTOR)) {
      console.log("PROJECTOR");
      layout = {
        'objAcross': 5,
        'objDown': 3,
        'spaceBtwObj': 600,
        'spaceBtwLayers': 2000,
        'cameraZ': 5400,
        'cameraX': 0,
        'cameraY': 0,
        'embedly' :
            {
              'maxwidth': 500,
              'maxheight': 500,
              'width': 500
            },
        'social' :
            {
              'offsetLocation': 0,
              'offsetAlign': -80
            }
      };
    }
    // big monitor
    if (Modernizr.mq(BIG_MONITOR)) {
      console.log("BIG_MONITOR");
      layout = {
        'objAcross': 5,
        'objDown': 3,
        'spaceBtwObj': 600,
        'spaceBtwLayers': 2000,
        'cameraZ': 5400,
        'cameraX': 0,
        'cameraY': 0,
        'embedly' :
            {
              'maxwidth': 500,
              'maxheight': 500,
              'width': 200
            },
        'social' :
            {
              'offsetLocation': 0,
              'offsetAlign': -80
            }
      };
    }
    // mac desktop
    if (Modernizr.mq(MACBOOK_PRO_DESKTOP)) {
      console.log("MACBOOK_PRO_DESKTOP");
      layout = {
        'objAcross': 5,
        'objDown': 3,
        'spaceBtwObj': 600,
        'spaceBtwLayers': 2000,
        'cameraZ': 5400,
        'cameraX': 0,
        'cameraY': 0,
        'embedly' :
            {
              'maxwidth': 500,
              'maxheight': 800,
              'width': 500
            },
        'social' :
            {
              'offsetLocation': 0,
              'offsetAlign': -80
            }
      };
    }
  }
  // iPhone4
  if (type === 'mobile') {
    // Landscape
    if (Modernizr.mq(MOBILE_LANDSCAPE)) {
      console.log("MOBILE_LANDSCAPE");
      layout = {
        'objAcross': 4,
        'objDown': 2,
        'spaceBtwObj': 600,
        'spaceBtwLayers': 2000,
        'cameraZ': 5400,
        'cameraX': 0,
        'cameraY': 0,
        'embedly' :
            {
              'maxwidth': 500,
              'maxheight': 500,
              'width': 200
            },
        'social' :
            {
              'offsetLocation': 0,
              'offsetAlign': -80
            }
      };
    }
    // Portrait
    if (Modernizr.mq(MOBILE_PORTRAIT)) {
      console.log("MOBILE_PORTRAIT");
      layout = {
        'objAcross': 5,
        'objDown': 3,
        'spaceBtwObj': 600,
        'spaceBtwLayers': 2000,
        'cameraZ': 5400,
        'cameraX': 0,
        'cameraY': 0,
        'embedly' :
            {
              'maxwidth': 500,
              'maxheight': 500,
              'width': 200
            },
        'social' :
            {
              'offsetLocation': 0,
              'offsetAlign': -80
            }
      };
    }
  }
  // iPad 2
  if (type === 'tablet') {
    // Landscape
    if (Modernizr.mq(TABLET_LANDSCAPE)) {
      console.log("TABLET_LANDSCAPE");
      layout = {
        'objAcross': 3,
        'objDown': 2,
        'spaceBtwObj': 600,
        'spaceBtwLayers': 2000,
        'cameraZ': 15093,
        'cameraX': 0,
        'cameraY': 0,
        'embedly' :
            {
              'maxwidth': 500,
              'maxheight': 500,
              'width': 500
            },
        'social' :
            {
              'offsetLocation': 0,
              'offsetAlign': -80
            }
      };
    }
    // Portrait
    if (Modernizr.mq(TABLET_PORTRAIT)) {
      console.log("TABLET_PORTRAIT");
      layout = {
        'objAcross': 3,
        'objDown': 2,
        'spaceBtwObj': 600,
        'spaceBtwLayers': 2000,
        'cameraZ': 5400,
        'cameraX': 0,
        'cameraY': 0,
        'embedly' :
            {
              'maxwidth': 500,
              'maxheight': 500,
              'width': 200
            },
        'social' :
            {
              'offsetLocation': 0,
              'offsetAlign': -80
            }
      };
    }
  }

  // TV
  if (type === 'tv') {
    layout = {
      'objAcross': 3,
      'objDown': 2,
      'spaceBtwObj': 600,
      'spaceBtwLayers': 2000,
      'cameraZ': 5400,
        'cameraX': 0,
        'cameraY': 0,
      'embedly' :
          {
            'maxwidth': 500,
            'maxheight': 500,
            'width': 200
          },
      'social' :
          {
            'offsetLocation': 0,
            'offsetAlign': -80
          }
    };
  }

  return layout;
};
