/**
 * @author julia_jacobs@labs.att.com (Julia Jacobs)
 * Date: 6/6/13
 * Time: 12:46 PM
 * @file names.js
 * @desc Embedly supported video providers.
 */

// 'use strict';


/**
 * @public
 * @function all
 * @memberof poc.Data.Providers.Names
 * @return {Array}
 */
poc.Data.Providers.Names.all = [
  'youtube', 'justintv', 'twitchtv', 'ustream', 'qik', 'revision3',
  'dailymotion', 'collegehumor', 'twitvid', 'telly',
  'myspacevideos',
  'metacafe', 'bliptv', 'googlevideo', 'revver', 'yahoovideo', 'viddler',
  'liveleak', 'animoto', 'dotsub',
  'overstream', 'livestream',
  'worldstarhiphop', 'bambuser', 'schooltube', 'bigthink', 'jibjab',
  'xtranormal', 'socialcam', 'dipdive', 'youku',
  'snotr', 'jardenberg',
  'clipfish', 'myvideo', 'vzaar', 'coub', 'streamio', 'vine', 'viddy',
  'whitehouse', 'hulu', 'crackle', 'fancast',
  'funnyordie', 'vimeo', 'ted',
  'nfb', 'thedailyshow', 'yahoomovies', 'colbertnation', 'comedycentral',
  'theonion', 'wordpresstv', 'traileraddict',
  'escapistmagazine', 'trailerspy', 'atom', 'foratv', 'spike', 'gametrailers',
  'koldcasttv', 'mixergy', 'pbsvideo',
  'zapiks', 'digg', 'trutv',
  'nzonscreen', 'wistia', 'hungrynation', 'indymogul', 'channelfrederator',
  'tmiweekly', '99dollarmusicvideos',
  'ultrakawaii',
  'barelypolitical', 'barelydigital', 'threadbanger', 'vodcars', 'confreaks',
  'allthingsd', 'nymag', 'aniboom',
  'grindtv', 'ifoodtv',
  'logotv', 'lonelyplanet', 'streetfire', 'trooptube', 'sciencestage',
  'brightcove', 'wirewax', 'canalplus', 'vevo',
  'pixorial',
  'spreecast', 'showme', 'looplogic', 'onaol', 'videodetective', 'khanacademy',
  'vidyard', 'godtube', 'tangle',
  'mediamatters',
  'clikthrough', 'clipsyndicate', 'espn', 'abcnews', 'washingtonpost', 'boston',
  'facebook', 'cnbc', 'cbsnews',
  'googleplus', 'cnn',
  'cnnedition', 'cnnmoney', 'msnbc', 'globalpost', 'guardian', 'bravotv',
  'nationalgeographic', 'discovery', 'forbes',
  'distrify',
  'foxnews', 'foxbusiness'
];


/**
 * @public
 * @function chosen
 * @memberof poc.Data.Providers.Names
 * @return {Array}
 */
poc.Data.Providers.Names.chosen = [
  'youtube', 'thedailyshow', 'comedycentral', 'spike', 'pbsvideo', 'espn',
  'abcnews', 'washingtonpost',
  'cnnedition', 'msnbc', 'bravotv', 'nationalgeographic', 'discovery', 'forbes',
  'foxnews'
];


/**
 * @public
 * @function enabled
 * @memberof poc.Data.Providers.Names
 * @return {Array}
 */
poc.Data.Providers.Names.enabled = [
  'youtube'
];
