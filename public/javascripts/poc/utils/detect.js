/**
 * @author julia_jacobs@labs.att.com (Julia Jacobs)
 * Date: 6/6/13
 * Time: 3:33 PM
 * @file detect.js
 * @desc Used for detection gateway page.
 */

// 'use strict';


/**
 * @public
 * @function variables
 * @memberof poc.Utils.Detect
 * @this variables
 * @return {Object}
 * @desc Used to detet devices,
 * browsers and operating systems.
 */
poc.Utils.Detect.variables = function() {
  var self = this, detectizr;

  Modernizr.Detectizr.detect({
    addAllFeaturesAsClass: true,
    detectDevice: true,
    detectModel: true,
    detectScreen: true,
    detectOS: true,
    detectBrowser: true,
    detectPlugins: true
  });

  self.device = Modernizr.Detectizr.device;
  self.isChrome = (self.device.browser === 'chrome');
  self.isDesktop = (self.device.type === 'desktop');
  self.isChromeDesktop = (self.isChrome && self.isDesktop);
  self.isTablet = (self.device.type === 'tablet');
  self.isTv = (self.device.type === 'tv');
  self.isMobile = (self.device.type === 'mobile');
  self.isLandscape = (self.device.orientation === 'landscape');
  self.isPortrait = (self.device.orientation === 'landscape');
  self.isLandscapeTablet = (self.isTablet && self.isLandscape);
  self.isPortraitTablet = (self.isTablet && self.isPortrait);
  self.upperCase = poc.Utils.Strings.uppercaseMe;

  return self;

};


/**
* @public
* @function requiredAppFeatures
* @return {string}
* @desc Generates message detailing required features
* for the application if not met by client.
*/
function requiredAppFeatures() {
  var doesNotHaveFeatures = [],
      vars = poc.Utils.Detect.variables(),
      result,

      /** features required by app */
      requiredFeatures = {
        'CSS 3D Transforms': Modernizr.csstransforms3d,
        'CSS Transitions': Modernizr.csstransitions,
        'Hash Change': Modernizr.hashchange,
        'CSS Animations': Modernizr.cssanimations,
        'CSS Opacity': Modernizr.opacity,
        'CSS Font Face': Modernizr.fontface,
        'HTML5 Video API': Modernizr.video,
        'HTML5 Localstorage API': Modernizr.localstorage
      },

      /** describes model and os */
      os = (vars.device.os) ? ' for ' +
      vars.upperCase(vars.device.os) : '',
      model = (vars.device.model) ? ' ' +
      vars.upperCase(vars.device.model) : '',

      /** information about what IS supported */
      notSupported = '<p>Sorry ' + vars.upperCase(vars.device.browser) +
      os + model +
      ' is not supported for this app.</p>',
      SUPPORTED = 'Please use one of these supported devices / browsers:' +
      '<ul><li>Latest version of Chrome</li>' +
      '<li>Samsung SmartTV browser (although the 3D ' +
      'navigation does not work yet)</li>' +
      '<li>iPad in landscape mode (not very well here Im ' +
      'afraid since fullscreen mode is only supported <br />' +
      'when a website is added to the Home Screen)</li></ul>',
      VISITING = '<p>Visiting this site with one of those devices / ' +
      'browsers will give you a link to go to the app and' +
      '<br />directions on how to use it.</p>' +
      '<p>I know its a pain but I hope you wont be disappointed! ' +
      'Thanks for checking this out.',
      message = notSupported + '<p>There are a number of mostly experimental ' +
      'HTML5 CSS3 features that your browser<b/>' +
      'does not support.</p><p>Namley:</p><ul>';

  _.each(requiredFeatures, function(value, key) {
    if (!value && !vars.isTv) {
      doesNotHaveFeatures.push(key);
    }
  });

  if (doesNotHaveFeatures.length > 0) {
    _.each(doesNotHaveFeatures, function(featureName) {
      //noinspection ReuseOfLocalVariableJS
      message = '<li>' + featureName + '</li>';
    });
    result = message + SUPPORTED + VISITING;
  } else {
    result = notSupported + SUPPORTED + VISITING;
  }

  return result;

}


/**
* @public
* @function ipadInLandscapeOrientation
* @return {string}
* @desc Generates message to ensure
* proper orientation.
*/
function ipadInLandscapeOrientation() {
  var IPAD = 'This app supports the iPad although you need to ' +
      'be in landscape orientation.  Please<br />' +
      'do so and refresh the website. ' +
      '<p>For best results add the ' +
      'site to the Home Screen.  <br /> ' +
      'This is the only way iOS devices will' +
      'open up a web page in fullscreen mode.  The site layout ' +
      'has been designed for fullscreen mode.',
      VISTING = '<p>Visiting this site in landscape orientation ' +
      'mode will give you a link to go to the app and' +
      '<br />directions on how to use it.</p>' +
      '<p>I know its a pain but I hope you wont be ' +
      'disappointed! Thanks for checking this out.',
      vars = poc.Utils.Detect.variables();

  return (vars.isPortraitTablet) ? IPAD + VISTING : '';
}


/**
* @public
* @function requirements
* @desc Generates app requirements gateway page.
*/
function requirements()
{
  //noinspection GjsLint
  var ALL = 'Welcome!  You are all set to see the app!',
      bugWarning = '<div style="color: #ff0000">Warning: this is a POC and is therefore extreamly buggy.' +
        '<br />Any venturing off of the directions listed below will result<br /> in super fun bug time.</div>',
      instructionArray = ['Accept permission to run app in full screen mode',
      'Let globe animation load and run (takes a few seconds)',
      'Click the <b>Search</b> menu icon',
      'Click the <b>YouTube</b> icon above the search input to check it',
      'Type a search query in the search input',
      'Navigate through 3D video search results grid using a trackpad, trackball or mouse wheel. <br />For iPad use pinch zoom.',
       'Click one of the poster images to bring up a player and play the video'],
      GO_TO_APP_LABEL = 'Go to app',
      CODE_AND_DOC_LABEL = 'Code and documentation',
      DOC_URL = 'docs',
      // cacheing jquery selectors
      $hOne = $('h1'),
      $newHeaderTwo = $('<h2 />'),
      $hTwo = $('h2'),
      $warning = $('#warning'),
      $newPara = $('<p />'),
      $device_info = $('#device_info'),
      $app = $('#app'),
      $info = $('#info'),
      $window_info = $('#window_info'),
      $no_dice = $('#no_dice'),
      $window = $(window),

      // to get rid of chained call to
      // function code style errors
      appAttr = $app.attr('href', '/app/#/'),
      infoAttr = $info.attr('href', DOC_URL),
      windowInfo = '<dl><dt>Window Height</dt><dd>' + $window.height() +
      '</dd></dl>' +
      '<dl><dt>Window Width</dt><dd>' + $window.width() + '</dd></dl>',
      vars = poc.Utils.Detect.variables(),
      message;

  if (vars.isChromeDesktop || vars.isLandscapeTablet || vars.isTv) {
    $hOne.after(
        $newHeaderTwo.text(ALL)
    );

    $warning.html(bugWarning);

    var INSTRUCTIONS = '<ul>';

    _.each(instructionArray, function(instruction){
        INSTRUCTIONS += '<li>' + instruction + '</li>';
    })

    INSTRUCTIONS += '</ul>'

    $hTwo.after(
      $newPara.html(INSTRUCTIONS)
    );
    $device_info.prettify({highlight: true}, vars.device);
    appAttr.html(GO_TO_APP_LABEL);
    infoAttr.html(CODE_AND_DOC_LABEL);
    $app.on('click', function() {
      screenfull.request();
    });
  } else {
    message = ipadInLandscapeOrientation() + requiredAppFeatures();
    $no_dice.html(message);
  }
}


/**
* @public
* @function init
* @memberof poc.Utils.Detect
*/
poc.Utils.Detect.init = function() {
  requirements();
};
