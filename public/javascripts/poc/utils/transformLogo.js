/**
 * @author julia_jacobs@labs.att.com (Julia Jacobs)
 * Date: 6/3/13
 * Time: 11:29 AM
 * @file transformLogo.js
 * @desc Handles logo animation, size and positioning.
 */

// 'use strict';


/**
* @public
* @function init
* @this init
* @memberof poc.Utils.Transformlogo
* @return {Object}
*/
poc.Utils.Transformlogo.init = function() {
  var self = this,
      top,
      left,
      $animImage = $('.animImage'),
      $location = $(location),
      locationAttr = $location.attr('href'),
      vars = poc.Utils.Detect.variables();

  self.hrefArray = locationAttr.split('/');

  //noinspection NestedFunctionJS
  /**
    * @desc Determins what the position of the logo should be
    * when up in corner based on device and window size.
    * @public
    * @function devicePos
    * @memberof poc.Utils.Transformlogo.func.init
    */
  self.devicePos = function() {

    if (vars.isDesktop || vars.isLandscapeTablet) {
      top = '10%';
      left = '5%';
    }

    if (vars.isPortraitTablet) {
      top = '10%';
      left = '10%';
    }

    if (vars.isMobile) {
      top = '10%';
      left = '10%';
    }
  };
  //noinspection NestedFunctionJS
  /**
    * @desc Determins what the position of the logo should be
    * when up in corner based on device and window size.
    * @public
    * @function deviceTransit
    * @memberof init
    */
  self.deviceTransit = function() {
    $animImage.transition({ opacity: 1, scale: 30 }, 5000, 'ease');
    $animImage.transition(
        { top: top, left: left, scale: 10 },
        1000,
        'ease',
        function() {
          $location.attr('href', '#/menu');
        });
  };
  //noinspection NestedFunctionJS
  /**
    * @desc Sets end position of animation.
    * @public
    * @function deviceEndPos
    * @memberof poc.Utils.Transformlogo.func.init
    */
  self.deviceEndPos = function() {
    $animImage.css({
      top: top,
      left: left,
      opacity: 1,
      '-webkit-transform': 'scale(10, 10)'
    });
  };

  return self;
};


/**
* @public
* @function start
* @memberof poc.Utils.Transformlogo
*/
poc.Utils.Transformlogo.start = function() {
  var $window = $(window),
      init = poc.Utils.Transformlogo.init();

  init.devicePos();
  // animation only occurs on home page
  if (init.hrefArray[5] == '') {
    init.deviceTransit();
  } else {
    init.deviceEndPos();
  }

  $window.on('throttledresize', function() {
    init.deviceEndPos();
  });

};

