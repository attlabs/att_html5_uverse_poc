﻿;/**
 * @author julia_jacobs@labs.att.com (Julia Jacobs)
 * Date: 6/3/13
 * Time: 11:27 AM
 * @file router.js
 * @desc Sammy SPA router using {@link http://mustache.github.io/ mustache}
 * templates.
 */

/*globals Sammy, $ */

// 'use strict';


/**
 * @public
 * @function init
 * @memberof poc.Router
 * @todo Fix to swap out dom instead of
 * having to remove and recreate it on every page
 */
poc.Router.init = function() {

  var app = Sammy('#container', function() {
    var CONTAINER_FIRST_CHILD_SELECTOR = '#container > div:first-child',
        $main = $('#main'),
        $menu = $('#menu'),
        $search = $('#search'),
        $container = $('#container'),
        $index = $('#index'),
        $div = $('<div />'),
        $divWithId = $div.attr('id', 'main'),

      /**
       * @public
       * @function fadePageIn
       * @memberof poc.Router
       * @desc Fades new page when page is
       * accessed.
       */
        fadePageIn = function() {
          var elemObj = $(CONTAINER_FIRST_CHILD_SELECTOR);
          elemObj.hide();
          elemObj.fadeIn(3000);
        };

    this.use(Sammy.Mustache);
    this.get('#/', function() {
      localStorage.clear();
      this.partial('/index.ms', function(html) {
        $main.html(html);
      });
    });
    this.get('#/search', function() {
      $main.remove();
      $container.append($divWithId);
      this.partial('/search.ms', function(html) {
        $main.html(html);
      });
    });
    this.get('#/search/:query', function() {
      $search.remove();
      $container.append($divWithId);
      this.partial('/search.ms', function(html) {
        $main.html(html);
      });
    });
    this.get('#/settings', function() {
      $menu.remove();
      $container.append($divWithId);
      this.partial('/settings.ms', function(html) {
        $main.html(html);
      });
    });
    this.get('#/social', function() {
      $menu.remove();
      $container.append($divWithId);
      this.partial('/social.ms', function(html) {
        $main.html(html);
      });
    });
    this.get('#/library', function() {
      $menu.remove();
      $container.append($divWithId);
      this.partial('/library.ms', function(html) {
        $main.html(html);
      });
    });
    this.get('#/menu', function() {
      $index.remove();
      $container.append($divWithId);
      this.partial('/menu.ms', function(html) {
        $main.html(html);
        fadePageIn();
      });
    });

  });

  app.run('/app/#/');

};

