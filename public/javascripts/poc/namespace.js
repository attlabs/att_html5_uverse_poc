/**
* @author julia_jacobs@labs.att.com (Julia Jacobs)
* Date: 11/7/2013
* Time: 7:10 PM
* @file namespace.js
* @desc Application global namespaces.
*/

// 'use strict';


/**
* @namespace poc
*/
window.poc = {} || poc;


/**
* @namespace poc.Namespace;
* @memberof poc
*/
poc.Namespace = {};


/**
* @namespace poc.Router;
* @memberof poc
*/
poc.Router = {};


/**
* @namespace poc.Utils;
* @memberof poc
*/
poc.Utils = {};


/**
* @namespace poc.Utils.Detect;
* @memberof poc.Utils
*/
poc.Utils.Detect = {};


/**
* @namespace poc.Utils.Dom_manipulation;
* @memberof poc.Utils
*/
poc.Utils.Dom_manipulation = {};


/**
* @namespace poc.Utils.Strings;
* @memberof poc.Utils
*/
poc.Utils.Strings = {};


/**
* @namespace poc.Utils.Transformlogo;
* @memberof poc.Utils
*/
poc.Utils.Transformlogo = {};


/**
* @namespace poc.Models;
* @memberof poc
*/
poc.Models = {};


/**
* @namespace poc.Models.Factory;
* @memberof poc.Models
*/
poc.Models.Factory = {};


/**
* @namespace poc.Models.View;
* @memberof poc.Models
*/
poc.Models.View = {};


/**
* @namespace poc.Models.View.Menu;
* @memberof poc.Models.View
*/
poc.Models.View.Menu = {};


/**
* @namespace poc.Models.View.Search;
* @memberof poc.Models.View
*/
poc.Models.View.Search = {};


/**
* @namespace poc.Grid;
* @memberof poc
*/
poc.Grid = {};


/**
* @namespace poc.Grid.Details;
* @memberof poc.Grid
*/
poc.Grid.Details = {};


/**
* @namespace poc.Grid.Elements;
* @memberof poc.Grid
*/
poc.Grid.Elements = {};


/**
* @namespace poc.Grid.Layout;
* @memberof poc.Grid
*/
poc.Grid.Layout = {};


/**
* @namespace poc.Data;
* @memberof poc
*/
poc.Data = {};


/**
* @namespace poc.Data.Providers;
* @memberof poc.Data
*/
poc.Data.Providers = {};


/**
* @namespace poc.Data.Providers.Api;
* @memberof poc.Data.Providers
*/
poc.Data.Providers.Api = {};


/**
* @namespace poc.Data.Providers.Names;
* @memberof poc.Data.Providers
*/
poc.Data.Providers.Names = {};


/**
* @namespace poc.Data.Layout;
* @memberof poc.Data
*/
poc.Data.Layout = {};


/**
* @namespace poc.Data.Layout.Buttons;
* @memberof poc.Data.Layout
*/
poc.Data.Layout.Buttons = {};


/**
* @namespace poc.Data.Layout.Presets;
* @memberof poc.Data.Layout
*/
poc.Data.Layout.Presets = {};


/**
* @namespace poc.Data.Api;
* @memberof poc.Data
*/
poc.Data.Api = {};


/**
* @namespace poc.Data.Api.Data;
* @memberof poc.Data.Api
*/
poc.Data.Api.Data = {};


/**
* @namespace poc.Data.Api.Embedly;
* @memberof poc.Data.Api
*/
poc.Data.Api.Embedly = {};

