/**
 * @author julia_jacobs@labs.att.com (Julia Jacobs)
 * Date: 6/7/13
 * Time: 2:19 PM
 * @file details.js
 * @desc Used to generate details from video poster item pop-up.
 */

poc.Grid.Details = {};

poc.Grid.Details.panel = function (item) {

    var player = $(item[0].html)
        .css({
            'z-index':'800000'
        });

    var social = $('<div/>')
        .attr('id', 'social-share');

    var videoDetails = $("<div />")
    .addClass('video_detail_content detailspaceygreen')
    .css({
        width: item[0].width - 32
    })
    .append(
        $('<p />', {
            html : item[0].title
        }),
        $('<p />', {
            html : item[0].description
        }),
        $('<p />')
        .append($('<a />', {
                href: item[0].author_url,
                html: item[0].author_name
            })
        )
    );

    $('#opacity_background')
    .css({
        'background-color': 'black',
        opacity: '0.5',
        height: '100%',
        width: '100%'
    });

    $('#main_vid_info').css({
        position : 'absolute'
    });

    $('#video_info_container').css({
        position : 'absolute',
        left: Math.floor((window.innerWidth / 2) - (item[0].width / 2))
    })
    .append(
        $('<a/>')
        .attr('src', '#')
            .css({
                position: 'absolute',
                bottom: 0,
                right: 0
            })
        .append(
            $('<i />')
            .css({
                float: 'right',
                cursor: 'pointer'
            })
            .addClass("icon-remove-sign")
        )
        .on('click', function(){
                $('#video_info_container').fadeOut();
                $('#video_info_container').empty();
                $('#video_info_container').removeAttr('style');
                $('#main_vid_info').removeAttr('style');
                $(".icon-search").show();
                $(".icon-home").show();
            })
        , social
        , player
        , videoDetails
    )
    .fadeIn();

    $('#social-share').ready(function(){
        $('#social-share').dcSocialShare({
             buttons: 'twitter,facebook,plusone,email',
             size: 'vertical',
             txtEmail: 'Email',
             twitterId: 'AttUser1',
             email: 'julia_jacobs@labs.att.com',
             url: $('iframe').attr('src'),
             title: item[0].title,
             description: item[0].description,
             classWrapper: 'dcssb-float',
             classContent: 'dcssb-content',
             location: 'top',
             align: 'right',
             offsetLocation: poc.Data.Layout.Presets.settings().social.offsetLocation,
             offsetAlign: poc.Data.Layout.Presets.settings().social.offsetAlign,
             center: 0,
             speedFloat: 1500,
             speed: 600,
             floater: true,
             autoClose: false,
             loadOpen: true,
             easing: 'easeOutQuint',
             classOpen: 'dc-open',
             classClose: 'dc-close',
             classToggle: 'dc-toggle'
         });
    });

    $('.video_detail_content.detailspaceygreen').ready(function(){
        $('#video_info_container').css({
            height: $('.video_detail_content.detailspaceygreen').height() + item[0].height + 32,
            top: Math.floor(
                (window.innerHeight) / 2 -
                (($('.video_detail_content.detailspaceygreen').height() + item[0].height + 32) / 2)
            )
        })
    });
};
