﻿; /**
 * @author julia_jacobs@labs.att.com (Julia Jacobs)
 * Date: 6/3/13
 * Time: 12:56 PM
 * @file menu.js
 * @desc  Knockout.js Menu view model.
 */

// 'use strict';


/**
 * @public
 * @function viewModel
 * @this viewModel
 * @memberof  poc.Models.View.Menu
 * @return {Object}
 */
poc.Models.View.Menu.viewModel = function() {
  var self = this;
  var data = poc.Data.Layout.Buttons.menu;
  self.response = ko.mapping.fromJS(data);
  return self;
};


/**
 * @public
 * @function start
 * @memberof  poc.Models.View.Menu
 */
poc.Models.View.Menu.start = function() {
  var viewModel = poc.Models.View.Menu.viewModel();
  poc.Models.View.Menu.wireEvents();
  ko.applyBindings(viewModel, document.getElementById('menu'));
};


/**
 * @public
 * @function wireEvents
 * @memberof  poc.Models.View.Menu
 */
poc.Models.View.Menu.wireEvents = function() {
  var $document = $(document),
      $location = $(location),
      $icon = $('div[class*="icon-"]'),
      $label = $('.details');

  $document.on('click', function(e) {
    var target = e.target,
        $target = $(target),
        loc = $target.data('loc');

    if ($icon.is(target)) {
      $location.attr('href', loc);
    }

    if ($label.is(target)) {
      $location.attr('href', loc);
    }
  });
};

