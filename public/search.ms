<div id="search">
    <div id="video_container"></div>
    <div id="main_vid_info">
        <div id="video_info_container"></div>
        <div id="opacity_background"></div>
    </div>
    <div id="icon_nav" class="span6">
        <div class="text-right row-fluid span6">
            <div class="icon-search span3 text-right">
                <div class="details">Search</div>
            </div>
            <div class="icon-home span3 text-right">
                <div class="details">Home</a></div>
            </div>
        </div>
    </div>
    <div id="search_box">
        <div id="providers"></div>
        <div id="searchElem">
            <input id="query" type="text" value="">
            <button id="search_button">search</button>
        </div>
    </div>
    <script>
        $().ready(function(){
            $('.icon-search').on('click', function(){
                $('#search_box').slideToggle('slow');
            });
            poc.Models.View.Search.start();
        });
    </script>
</div>