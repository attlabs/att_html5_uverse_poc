========
The U-Verse POC
========
Using `knockoutjs`_  for binding API JSON data to HTML DOM elements and `sammy`_ for SPA page routing

Includes:
=========
  * `node.js`_ & `express`_ for the web server
  * `Twitter Bootstrap`_ for basic responsive styling
  * `jquery`_ with a bunch of jquery plugins
  * `less-middleware`_ - css
  * `threejs`_ for CSS3D rendering.
  * `haml`_ for express templates

and a bunch of other libraries.

Installing app
==============
Instructions on installing the app can be found in the `Wiki`_

.. GENERAL LINKS

.. _`knockoutjs`: http://knockoutjs.com/index.html
.. _`sammy`: http://http://sammyjs.org/
.. _`node.js`: http://nodejs.org/
.. _`less-middleware`: http://lesscss.org/
.. _`express`: http://expressjs.com/guide.html
.. _`Twitter Bootstrap`: http://twitter.github.com/bootstrap/
.. _`jquery`: http://jquery.com/
.. _`threejs`: https://github.com/mrdoob/three.js/
.. _`haml`: git://github.com/visionmedia/haml.js.git/
.. _`grunt` : http://gruntjs.com/
.. _`Wiki`: https://bitbucket.org/attlabs/att_html5_uverse_poc/wiki/Home


